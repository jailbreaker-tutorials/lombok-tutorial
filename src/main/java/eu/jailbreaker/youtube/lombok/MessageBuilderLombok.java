package eu.jailbreaker.youtube.lombok;

import lombok.Builder;

@Builder
public class MessageBuilderLombok {

    private final String base;
    private final boolean bold;
    private final boolean italic;
    private final boolean underlined;

    public String finish() {
        System.out.println("base = " + base);
        System.out.println("bold = " + bold);
        System.out.println("italic = " + italic);
        System.out.println("underlined = " + underlined);
        return this.base;
    }
}
