package eu.jailbreaker.youtube.lombok;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class TestEntityLombok {

    private long id;
    private String name;
    private int age;

}
