package eu.jailbreaker.youtube.lombok;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.util.UUID;

/**
 * @Data übernimmt lediglich {@Getter @Setter @RequiredArgsConstructor @ToString @EqualsAndHashCode}
 */
@Data
@AllArgsConstructor
public class PersonLombok {

    private final UUID uniqueId;

    private final String name;
    private final String secondName;

    @Accessors(fluent = true, chain = true)
    private int age;

    public PersonLombok duplicate(@NonNull PersonLombok lombok) {
        return new PersonLombok(lombok.uniqueId, lombok.name, lombok.secondName, lombok.age);
    }
}
