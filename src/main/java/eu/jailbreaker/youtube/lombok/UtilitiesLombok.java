package eu.jailbreaker.youtube.lombok;

import lombok.experimental.UtilityClass;

import java.util.AbstractMap;
import java.util.Map;

@UtilityClass
public class UtilitiesLombok {

    private final String TEST_ATTRIBUTE = "TEST";

    public <K, V> Map.Entry<K, V> createEntry(K key, V value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }
}
