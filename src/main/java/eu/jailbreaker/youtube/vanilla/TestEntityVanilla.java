package eu.jailbreaker.youtube.vanilla;

public class TestEntityVanilla {

    private long id;
    private String name;
    private int age;

    public TestEntityVanilla() {
    }

    public TestEntityVanilla(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
