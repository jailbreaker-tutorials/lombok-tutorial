package eu.jailbreaker.youtube.vanilla;

import java.util.AbstractMap;
import java.util.Map;

public final class UtilitiesVanilla {

    private static final String TEST_ATTRIBUTE = "TEST";

    private UtilitiesVanilla() {
        throw new UnsupportedOperationException("Von dieser Klasse darf es keine Instanz geben!");
    }

    public static <K, V> Map.Entry<K, V> createEntry(K key, V value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }
}
