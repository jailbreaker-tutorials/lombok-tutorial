package eu.jailbreaker.youtube.vanilla;

public class MessageBuilderVanilla {

    private final String base;
    private final boolean bold;
    private final boolean italic;
    private final boolean underlined;

    public MessageBuilderVanilla(String base, boolean bold, boolean italic, boolean underlined) {
        this.base = base;
        this.bold = bold;
        this.italic = italic;
        this.underlined = underlined;
    }

    public static MessageBuilder builder() {
        return new MessageBuilder();
    }

    public String finish() {
        System.out.println("base = " + base);
        System.out.println("bold = " + bold);
        System.out.println("italic = " + italic);
        System.out.println("underlined = " + underlined);
        return this.base;
    }

    public static class MessageBuilder {
        private String base;
        private boolean bold;
        private boolean italic;
        private boolean underlined;

        public MessageBuilder base(String base) {
            this.base = base;
            return this;
        }

        public MessageBuilder bold(boolean bold) {
            this.bold = bold;
            return this;
        }

        public MessageBuilder italic(boolean italic) {
            this.italic = italic;
            return this;
        }

        public MessageBuilder underlined(boolean underlined) {
            this.underlined = underlined;
            return this;
        }

        public MessageBuilderVanilla build() {
            return new MessageBuilderVanilla(this.base, this.bold, this.italic, this.underlined);
        }
    }
}
