package eu.jailbreaker.youtube.vanilla;

import java.util.Objects;
import java.util.UUID;

public class PersonVanilla {

    private final UUID uniqueId;

    private final String name;
    private final String secondName;

    private int age;

    public PersonVanilla(UUID id, String name, String secondName) {
        uniqueId = id;
        this.name = name;
        this.secondName = secondName;
    }

    public PersonVanilla(UUID uniqueId, String name, String secondName, int age) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.secondName = secondName;
        this.age = age;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // Nur mit @Accessors(fluent = true)
    public int age() {
        return age;
    }

    // Nur mit @Accessors(fluent = true, chain = true)
    public PersonVanilla age(int age) {
        this.age = age;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonVanilla vanilla = (PersonVanilla) o;
        return age == vanilla.age &&
                Objects.equals(uniqueId, vanilla.uniqueId) &&
                Objects.equals(name, vanilla.name) &&
                Objects.equals(secondName, vanilla.secondName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uniqueId, name, secondName, age);
    }

    @Override
    public String toString() {
        return "PersonVanilla{" +
                "uniqueId=" + uniqueId +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", age=" + age +
                '}';
    }

    public PersonVanilla duplicate(PersonVanilla vanilla) {
        if (vanilla == null) {
            throw new NullPointerException("Person is null");
        }
        return new PersonVanilla(vanilla.uniqueId, vanilla.name, vanilla.secondName, vanilla.age);
    }
}
