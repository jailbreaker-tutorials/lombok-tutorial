package eu.jailbreaker.youtube;

import eu.jailbreaker.youtube.lombok.MessageBuilderLombok;
import eu.jailbreaker.youtube.lombok.UtilitiesLombok;
import eu.jailbreaker.youtube.vanilla.MessageBuilderVanilla;
import eu.jailbreaker.youtube.vanilla.UtilitiesVanilla;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

public class YouTube {

    public static void main(String[] args) {
    }

    // @SneakyThrows lohnt sich, wenn im Catch-Block keine Rückgabe/Ausgabe außer des Stacktraces stattfindet
    @SneakyThrows
    private static void simulateSneakyThrows(File file) {
        @Cleanup
        FileReader reader = new FileReader(file);
        final String encoding = reader.getEncoding();
    }

    private static void simulateTryCatch(File file) {
        try (FileReader reader = new FileReader(file)) {
            final String encoding = reader.getEncoding();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void simulateValVar() {
        String vanillaString = "Vanilla_String";
        final String vanillaFinalString = "Vanilla_Final_String";

        val valString = "Val_String";
        System.out.println(valString);
        valString = "Val_String_Final_Test";
        System.out.println(valString);

        var varString = "Var_String";
        System.out.println(varString);
        varString = "Var_String_Final_Test";
        System.out.println(varString);
    }

    private static void simulateUtilities() {
        final Map.Entry<String, String> vanilla = UtilitiesVanilla.createEntry("Key", "Value");
        final Map.Entry<String, String> lombok = UtilitiesLombok.createEntry("Key", "Value");
    }

    private static void simulateMessageBuilder() {
        MessageBuilderLombok.builder()
                .base("Das ist eine TestNachricht")
                .bold(true)
                .italic(true)
                .underlined(true)
                .build()
                .finish();

        MessageBuilderVanilla.builder()
                .base("Das ist eine TestNachricht")
                .bold(true)
                .italic(true)
                .underlined(true)
                .build()
                .finish();
    }

    private static void simulateCleanup() {
        System.out.println("Close-Try-With-Resources");
        closeTryWithResources();
        System.out.println("Close-Annotation");
        closeAnnotation();
    }

    private static void closeAnnotation() {
        @Cleanup
        TestClose testClose = new TestClose();
        testClose.print();
    }

    private static void closeTryWithResources() {
        TestClose testClose = new TestClose();
        testClose.print();
        testClose.close();
    }

    // Muss nicht zwanghaft ein Closeable sein
    // Lombok benötigt lediglich eine Methode die "close()" heißt
    public static class TestClose {

        public void print() {
            System.out.println("Das ist eine Nachricht (" + System.currentTimeMillis() + ")");
        }

        public void close() {
            System.out.println("Ich wurde beendet [" + System.currentTimeMillis() + "]");
        }
    }
}
